 # Ohos-WM

 **本项目是基于开源项目AndroidWM进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/huangyz0918/AndroidWM )追踪到原项目版本**

 #### 项目介绍

 - 项目名称：Ohos-WM
 - 所属系列：ohos的第三方组件适配移植
 - 功能：一个轻量级的图片水印框架，支持隐形数字水印。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/huangyz0918/AndroidWM
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：无release版本, sha1:23d91a3767ce9b146cf3e3c62db5f8f0ee05c556

 #### 演示效果

<img src="screenshot/sample.gif"/>

 #### 安装教程

 1. 编译依赖库har包ohoswm.har。
 2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
 3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
 ```
 dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 	……
 }
 ```
 4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

 方法2.
 1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
 2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.watermark.ohos:ohoswm:1.0.0'
}
```

 #### 使用说明

 1. 生成图片水印
    
 ```java
    WatermarkImage watermarkImage = new WatermarkImage(watermarkBitmap)
                         .setImageAlpha(20)
                         .setPositionX(Math.random())
                         .setPositionY(Math.random())
                         .setRotation(30)
                         .setSize(0.1);
 
                 WatermarkBuilder
                         .create(this, backgroundView)
                         .loadWatermarkImage(watermarkImage)
                         .setTileMode(true)
                         .getWatermark()
                         .setToImageView(backgroundView);
 ```

 2. 生成隐形图片水印
    
 ```java
    WatermarkBuilder
        .create(this, backgroundView)
        .loadWatermarkImage(watermarkBitmap)
        .setInvisibleWMListener(true, new BuildFinishListener<PixelMap>() {
            @Override
            public void onSuccess(PixelMap pixelMap) {
               
            }

            @Override
            public void onFailure(String message) {
            
            }
        });
 ```

 3. 生成文字水印
    
 ```java
    WatermarkText watermarkText = new WatermarkText(editText.getText())
             .setPositionX(0.5)
             .setPositionY(0.5)
             .setRotation(40)
             .setTextAlpha(150)
             .setTextColor(Color.DKGRAY.getValue())
             .setTextShadow(0.1f, 5, 5, Color.BLUE.getValue());
    
    WatermarkBuilder.create(this, backgroundView)
             .setTileMode(true)
             .loadWatermarkText(watermarkText)
             .getWatermark()
             .setToImageView(backgroundView);
 ```

 4. 生成隐形文字水印
    
 ```java
    WatermarkText watermarkText1 = new WatermarkText(editText.getText());
    WatermarkBuilder
            .create(this, backgroundView)
            .loadWatermarkText(watermarkText1)
            .setInvisibleWMListener(true, new BuildFinishListener<PixelMap>() {
                @Override
                public void onSuccess(PixelMap object) {
                  
                }
    
                @Override
                public void onFailure(String message) {
                  
                }
            }); 
 ```

  5 检测隐形水印；
```java
   WatermarkDetector.create(backgroundView, true)
           .detect(new DetectFinishListener() {
               @Override
               public void onSuccess(DetectionReturnValue returnValue) {
                
               }
   
               @Override
               public void onFailure(String message) {
                 
               }
           });
```

 #### 版本迭代

 - v1.0.0

  实现功能

   1. 文字水印。
   2. 图片水印。
   3. 隐形图片水印。
   4. 隐形文字水印。

 #### 版权和许可信息
 - Copyright 2018 Yizheng Huang

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.



