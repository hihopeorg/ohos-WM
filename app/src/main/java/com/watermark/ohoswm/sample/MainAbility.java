package com.watermark.ohoswm.sample;

import com.watermark.ohoswm.WatermarkBuilder;
import com.watermark.ohoswm.WatermarkDetector;
import com.watermark.ohoswm.bean.WatermarkImage;
import com.watermark.ohoswm.bean.WatermarkText;
import com.watermark.ohoswm.listener.BuildFinishListener;
import com.watermark.ohoswm.listener.DetectFinishListener;
import com.watermark.ohoswm.task.DetectionReturnValue;
import com.watermark.ohoswm.utils.BitmapUtils;
import es.dmoral.toasty.Toasty;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;

public class MainAbility extends Ability implements Component.ClickedListener {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "MainAbility");

    private Button btnAddText;
    private Button btnAddImg;
    private Button btnAddInVisibleImage;
    private Button btnAddInvisibleText;
    private Button btnDetectImage;
    private Button btnDetectText;
    private Button btnClear;

    private Image backgroundView;
    private Image watermarkView;
    private PixelMap watermarkBitmap;

    private TextField editText;

    private ProgressBar progressBar;

    public String detectText="";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initViews();
        initEvents();
    }

    private void initViews() {
        btnAddImg = (Button) findComponentById(ResourceTable.Id_btn_add_image);
        btnAddText = (Button) findComponentById(ResourceTable.Id_btn_add_text);
        btnAddInVisibleImage = (Button) findComponentById(ResourceTable.Id_btn_add_invisible_image);
        btnAddInvisibleText = (Button) findComponentById(ResourceTable.Id_btn_add_invisible_text);
        btnDetectImage = (Button) findComponentById(ResourceTable.Id_btn_detect_image);
        btnDetectText = (Button) findComponentById(ResourceTable.Id_btn_detect_text);
        btnClear = (Button) findComponentById(ResourceTable.Id_btn_clear_watermark);

        editText = (TextField) findComponentById(ResourceTable.Id_editText);
        backgroundView = (Image) findComponentById(ResourceTable.Id_imageView);
        watermarkView = (Image) findComponentById(ResourceTable.Id_imageView_watermark);
        progressBar = (ProgressBar) findComponentById(ResourceTable.Id_progressBar);
        watermarkBitmap = BitmapUtils.createPixmapByResourceId(this, ResourceTable.Media_test_watermark);
        watermarkView.setVisibility(Component.HIDE);
    }

    private void initEvents() {
        // The sample method of adding a text watermark.
        btnAddText.setClickedListener(this);
        // The sample method of adding an image watermark.
        btnAddImg.setClickedListener(this);
        // The sample method of adding an invisible image watermark.
        btnAddInVisibleImage.setClickedListener(this);
        // The sample method of adding an invisible text watermark.
        btnAddInvisibleText.setClickedListener(this);
        // detect the text watermark.
        btnDetectText.setClickedListener(this);
        // detect the image watermark.
        btnDetectImage.setClickedListener(this);
        // reload the background.
        btnClear.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_add_text:
                WatermarkText watermarkText = new WatermarkText(editText.getText())
                        .setPositionX(0.5)
                        .setPositionY(0.5)
                        .setRotation(40)
                        .setTextAlpha(0.4f)
                        .setTextColor(Color.BLUE.getValue())
                        .setTextShadow(0.1f, 5, 5, Color.BLUE.getValue());

                WatermarkBuilder.create(this, backgroundView)
                        .setTileMode(true)
                        .loadWatermarkText(watermarkText)
                        .getWatermark()
                        .setToImageView(backgroundView);
                break;
            case ResourceTable.Id_btn_add_image:
                // Math.random()
                WatermarkImage watermarkImage = new WatermarkImage(watermarkBitmap)
                        .setImageAlpha(0.4f)
                        .setPositionX(Math.random())
                        .setPositionY(Math.random())
                        .setRotation(30)
                        .setSize(0.1);

                WatermarkBuilder
                        .create(this, backgroundView)
                        .loadWatermarkImage(watermarkImage)
                        .setTileMode(true)
                        .getWatermark()
                        .setToImageView(backgroundView);
                break;
            case ResourceTable.Id_btn_add_invisible_image:
                progressBar.setVisibility(Component.VISIBLE);
                WatermarkBuilder
                        .create(this, backgroundView)
                        .loadWatermarkImage(watermarkBitmap)
                        .setInvisibleWMListener(true, new BuildFinishListener<PixelMap>() {
                            @Override
                            public void onSuccess(PixelMap pixelMap) {
                                progressBar.setVisibility(Component.HIDE);
                                Toasty.success(getContext(), "Successfully create invisible watermark!", 0, true);
                                if (pixelMap != null) {
                                    backgroundView.setBackground(new PixelMapElement(pixelMap));
                                }
                            }

                            @Override
                            public void onFailure(String message) {
                                progressBar.setVisibility(Component.HIDE);
                                HiLog.error(label, message);
                            }
                        });
                break;
            case ResourceTable.Id_btn_add_invisible_text:
                progressBar.setVisibility(Component.VISIBLE);
                WatermarkText watermarkText1 = new WatermarkText(editText.getText());
                WatermarkBuilder
                        .create(this, backgroundView)
                        .loadWatermarkText(watermarkText1)
                        .setInvisibleWMListener(true, new BuildFinishListener<PixelMap>() {
                            @Override
                            public void onSuccess(PixelMap object) {
                                progressBar.setVisibility(Component.HIDE);
                                Toasty.info(getContext(), "Successfully create invisible watermark!", 0, true);
                                if (object != null) {
                                    backgroundView.setBackground(new PixelMapElement(object));
                                }
                            }

                            @Override
                            public void onFailure(String message) {
                                progressBar.setVisibility(Component.HIDE);
                                HiLog.error(label, message);
                            }
                        });
                break;
            case ResourceTable.Id_btn_detect_text:
                progressBar.setVisibility(Component.VISIBLE);
                WatermarkDetector.create(backgroundView, true)
                        .detect(new DetectFinishListener() {
                            @Override
                            public void onSuccess(DetectionReturnValue returnValue) {
                                progressBar.setVisibility(Component.HIDE);
                                if (returnValue != null) {
                                    Toasty.info(getContext(), "Successfully detected invisible text: "
                                            + returnValue.getWatermarkString(), 0, true);
                                    detectText=returnValue.getWatermarkString();
                                }
                            }

                            @Override
                            public void onFailure(String message) {
                                detectText="";
                                progressBar.setVisibility(Component.HIDE);
                                HiLog.error(label, message);
                            }
                        });
                break;
            case ResourceTable.Id_btn_detect_image:
                progressBar.setVisibility(Component.VISIBLE);
                WatermarkDetector.create(backgroundView, true)
                        .detect(new DetectFinishListener() {
                            @Override
                            public void onSuccess(DetectionReturnValue returnValue) {
                                progressBar.setVisibility(Component.HIDE);
                                Toasty.info(getContext(), "Successfully detected invisible watermark!", 0, true);
                                if (returnValue != null) {
                                    watermarkView.setVisibility(Component.VISIBLE);
                                    watermarkView.setPixelMap(returnValue.getWatermarkBitmap());
                                }
                            }

                            @Override
                            public void onFailure(String message) {
                                progressBar.setVisibility(Component.HIDE);
                                HiLog.error(label, message);
                            }
                        });
                break;
            case ResourceTable.Id_btn_clear_watermark:
                backgroundView.setBackground(new PixelMapElement(BitmapUtils.createPixmapByResourceId(this, ResourceTable.Media_test2)));
                watermarkView.setVisibility(Component.HIDE);
                break;
        }
    }
}
