/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.watermark.ohoswm.sample;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.TextField;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.watermark.ohoswm.sample.EventHelper.triggerClickEvent;
import static com.watermark.ohoswm.sample.EventHelper.waitForActive;

public class ExampleOhosTest {
    MainAbility ability;

    @Before
    public void startUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        waitForActive(ability, 1000);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(1000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testWatermarkInvisibleText() {
        Assert.assertEquals("",ability.detectText);
        TextField text = (TextField) ability.findComponentById(ResourceTable.Id_editText);
        text.setText("测试");
        Button btnAddInvisibleText = (Button) ability.findComponentById(ResourceTable.Id_btn_add_invisible_text);
        triggerClickEvent(ability, btnAddInvisibleText);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Button detectImage = (Button) ability.findComponentById(ResourceTable.Id_btn_detect_text);
        triggerClickEvent(ability, detectImage);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       Assert.assertEquals("测试",ability.detectText);
    }

    @Test
    public void testWatermarkInvisibleImage() {
        Image watermarkView = (Image) ability.findComponentById(ResourceTable.Id_imageView_watermark);
        Assert.assertTrue(watermarkView.getVisibility()== Component.HIDE);
        Button addInvisibleImage = (Button) ability.findComponentById(ResourceTable.Id_btn_add_invisible_image);
        triggerClickEvent(ability, addInvisibleImage);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Button detectImage = (Button) ability.findComponentById(ResourceTable.Id_btn_detect_image);
        triggerClickEvent(ability, detectImage);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       Assert.assertTrue(watermarkView.getVisibility()== Component.VISIBLE);
    }

}