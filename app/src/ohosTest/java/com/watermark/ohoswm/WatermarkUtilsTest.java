/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.watermark.ohoswm;

import com.watermark.ohoswm.sample.ResourceTable;
import com.watermark.ohoswm.utils.BitmapUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.junit.Assert;
import org.junit.Test;

public class WatermarkUtilsTest {

    @Test
    public void CreatePixmap() {
        PixelMap pixmap = BitmapUtils.createPixmap(PixelFormat.ARGB_8888, 300, 400);
        Assert.assertEquals(300, pixmap.getImageInfo().size.width);
    }

    @Test
    public void createPixmapByResourceId() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        PixelMap pixmap = BitmapUtils.createPixmapByResourceId(context, ResourceTable.Media_test_watermark);
        Assert.assertEquals(PixelFormat.ARGB_8888.getValue(), pixmap.getImageInfo().pixelFormat.getValue());
    }


    @Test
    public void bitmapToString() {
        PixelMap pixmap = BitmapUtils.createPixmap(PixelFormat.ARGB_8888, 50, 100);
        String pixMapStr = BitmapUtils.bitmapToString(pixmap);
        PixelMap pixelMap = BitmapUtils.stringToBitmap(pixMapStr);
        Assert.assertEquals(50, pixelMap.getImageInfo().size.width);
    }


    @Test
    public void stringToBitmap() {
        PixelMap pixmap = BitmapUtils.createPixmap(PixelFormat.ARGB_8888, 100, 200);
        String pixMapStr = BitmapUtils.bitmapToString(pixmap);
        PixelMap pixelMap = BitmapUtils.stringToBitmap(pixMapStr);
        Assert.assertEquals(100, pixelMap.getImageInfo().size.width);
    }


}
