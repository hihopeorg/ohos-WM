/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm.utils;

import com.watermark.ohoswm.bean.WatermarkImage;
import com.watermark.ohoswm.bean.WatermarkText;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.text.SimpleTextLayout;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.configuration.DeviceCapability;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ColorSpace;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Locale;

/**
 * Util class for operations with {@link PixelMap}.
 *
 * @author huangyz0918
 */
public class BitmapUtils {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "BitmapUtils");

    /**
     * build a bitmap from a text.
     *
     * @return {@link PixelMap} the bitmap return.
     */
    public static PixelMap textAsBitmap(Context context, WatermarkText watermarkText) {
        Paint watermarkPaint = new Paint();
        watermarkPaint.setColor(new Color(watermarkText.getTextColor()));
        watermarkPaint.setStyle(watermarkText.getTextStyle());

        if (watermarkText.getTextAlpha() >= 0 && watermarkText.getTextAlpha() <= 1) {
            watermarkPaint.setAlpha(watermarkText.getTextAlpha());
        }

        float value = (float) watermarkText.getTextSize();
        int pixel = (int) value * context.getResourceManager().getDeviceCapability().screenDensity / DeviceCapability.SCREEN_MDPI;
        watermarkPaint.setTextSize(pixel);

        watermarkPaint.setAntiAlias(true);
        watermarkPaint.setTextAlign(TextAlignment.LEFT);
        watermarkPaint.setStrokeWidth(10);

        float baseline = (int) (-watermarkPaint.ascent() + 1f);
        ohos.agp.utils.Rect bounds = watermarkPaint.getTextBounds(watermarkText.getText());
        int boundWidth = bounds.getWidth() + 20;
        int mTextMaxWidth = (int) watermarkPaint.measureText(watermarkText.getText());
        if (boundWidth > mTextMaxWidth) {
            boundWidth = mTextMaxWidth;
        }
        SimpleTextLayout staticLayout = new SimpleTextLayout(watermarkText.getText(), watermarkPaint,
                new ohos.agp.utils.Rect(0, 0, 0, 0)
                , mTextMaxWidth);

        int lineCount = staticLayout.getLineCount();
        int height = (int) (baseline + watermarkPaint.descent() + 3) * lineCount;
        PixelMap image = createPixmap(PixelFormat.ARGB_8888, 1, 1);
        if (boundWidth > 0 && height > 0) {
            image = createPixmap(PixelFormat.ARGB_8888, boundWidth, height);
        }
        Texture texture = new Texture(image);
        Canvas canvas = new Canvas(texture);
        canvas.drawColor(watermarkText.getBackgroundColor(), Canvas.PorterDuffMode.SRC);
        staticLayout.drawText(canvas);
        return image;
    }

    /**
     * this method is for image resizing, we should get
     * the size from the input {@link WatermarkImage}
     * objects, and, set the size from 0 to 1 ,which means:
     * size = watermarkImageWidth / backgroundImageWidth
     *
     * @return {@link PixelMap} the new bitmap.
     */
    public static PixelMap resizeBitmap(PixelMap watermarkImg, float size, PixelMap backgroundImg) {
        int bitmapWidth = watermarkImg.getImageInfo().size.width;
        int bitmapHeight = watermarkImg.getImageInfo().size.height;
        float scale = (backgroundImg.getImageInfo().size.width * size) / bitmapWidth;
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        return BitmapUtils.createPixmap(watermarkImg, 0, 0,
                bitmapWidth, bitmapHeight, matrix, true);
    }

    /**
     * this method is for image resizing, used in invisible watermark
     * creating progress. To make the progress faster, we should do
     * some pre-settings, user can set whether to do this part.
     * <p>
     * We set the new {@link PixelMap} to a fixed width = 512 pixels.
     *
     * @return {@link PixelMap} the new bitmap.
     */
    public static PixelMap resizeBitmap(PixelMap inputBitmap, int maxImageSize) {
        float ratio = Math.min(
                (float) maxImageSize / inputBitmap.getImageInfo().size.width,
                (float) maxImageSize / inputBitmap.getImageInfo().size.height);
        int width = Math.round(ratio * inputBitmap.getImageInfo().size.width);
        int height = Math.round(ratio * inputBitmap.getImageInfo().size.height);
        return BitmapUtils.createScaledBitmap(inputBitmap, width, height, true);
    }

    public static PixelMap createScaledBitmap(PixelMap src, int dstWidth, int dstHeight, boolean filter) {
        Matrix m = new Matrix();
        final int width = src.getImageInfo().size.width;
        final int height = src.getImageInfo().size.height;
        if (width != dstWidth || height != dstHeight) {
            final float sx = dstWidth / (float) width;
            final float sy = dstHeight / (float) height;
            m.setScale(sx, sy);
        }
        return BitmapUtils.createPixmap(src, 0, 0, width, height, m, filter);
    }

    /**
     * Convert a Bitmap to a String.
     */
    public static String bitmapToString(PixelMap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImagePacker imagePacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.format = "image/jpeg";
        packingOptions.quality = 100;
        imagePacker.initializePacking(bos, packingOptions);
        imagePacker.addImage(bitmap);
        imagePacker.finalizePacking();
        byte[] b = bos.toByteArray();
        return Base64.getEncoder().encodeToString(b);
    }

    /**
     * Convert a String to a Bitmap.
     *
     * @return bitmap (from given string)
     */
    public static PixelMap stringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.getDecoder().decode(encodedString);
            ByteArrayInputStream drawableInputStream = null;
            try {
                drawableInputStream = new ByteArrayInputStream(encodeByte);
                ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
                sourceOptions.formatHint = "image/jpeg";
                ImageSource imageSource = ImageSource.create(drawableInputStream, sourceOptions);
                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                decodingOptions.desiredSize = new Size(0, 0);
                decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
                decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                return pixelMap;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (drawableInputStream != null) {
                        drawableInputStream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (Exception e) {
            HiLog.error(label, e.toString());
            return null;
        }
    }

    /**
     * Saving a bitmap instance into local PNG.
     */
    public static boolean saveAsPNG(PixelMap mPixelMap, String path, boolean withTime) {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.CHINA).format(Calendar.getInstance().getTime());
        String filePath;
        if (withTime) {
            filePath = path + timeStamp + ".png";
        } else {
            filePath = path + "watermarked" + ".png";
        }
        ImagePacker imagePacker = ImagePacker.create();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.format = "image/jpeg";
            packingOptions.quality = 100;
            imagePacker.initializePacking(bos, packingOptions);
            imagePacker.addImage(mPixelMap);
            imagePacker.finalizePacking();
            byte[] buffer = bos.toByteArray();
            if (buffer != null) {
                File file = new File(filePath);
                if (file.exists()) {
                    file.delete();
                }
                OutputStream outputStream = new FileOutputStream(file);
                outputStream.write(buffer);
                outputStream.close();
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            HiLog.info(label, "IOException");
            return false;
        }
    }


    /**
     * convert the background bitmap into pixel array.
     */
    public static int[] getBitmapPixels(PixelMap inputBitmap) {
        int[] backgroundPixels = new int[inputBitmap.getImageInfo().size.width * inputBitmap.getImageInfo().size.height];
        inputBitmap.readPixels(backgroundPixels, 0
                , inputBitmap.getImageInfo().size.width
                , new Rect(0, 0
                        , inputBitmap.getImageInfo().size.width
                        , inputBitmap.getImageInfo().size.height));
        return backgroundPixels;
    }


    /**
     * Bitmap to Pixels then converting it to an ARGB int array.
     */
    public static int[] pixel2ARGBArray(int[] inputPixels) {
        int[] bitmapArray = new int[4 * inputPixels.length];
        for (int i = 0; i < inputPixels.length; i++) {
            bitmapArray[4 * i] = inputPixels[i] >>> 24;
            bitmapArray[4 * i + 1] = (inputPixels[i] >> 16) & 0xFF;
            bitmapArray[4 * i + 2] = (inputPixels[i] >> 8) & 0xFF;
            bitmapArray[4 * i + 3] = inputPixels[i] & 0xFF;
        }
        return bitmapArray;
    }

    public static PixelMap createPixmap(PixelFormat pixelFormat, int width, int height) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(width, height);
        initializationOptions.pixelFormat = pixelFormat;
        PixelMap pixelMap = PixelMap.create(initializationOptions);
        return pixelMap;
    }

    public static PixelMap createPixmap(PixelMap source, int x, int y, int width, int height, Matrix matrix, boolean isFilter) {
        int newW = width;
        int newH = height;
        PixelMap bitmap;
        Paint paint;
        RectFloat srcR = new RectFloat(x, y, x + width, y + height);
        RectFloat dstR = new RectFloat(0, 0, width, height);
        RectFloat deviceR = new RectFloat();
        ColorSpace cs = source.getImageInfo().colorSpace;

        if (matrix == null) {
            paint = null;   // not needed
        } else {
            matrix.mapRect(deviceR, dstR);
            newW = Math.round(deviceR.getWidth());
            newH = Math.round(deviceR.getHeight());
            paint = new Paint();
            paint.setFilterBitmap(isFilter);
            paint.setAntiAlias(true);
        }

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(newW, newH);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        bitmap = PixelMap.create(initializationOptions);
        bitmap.getImageInfo().colorSpace = cs;
        bitmap.setBaseDensity(source.getBaseDensity());

        Texture texture = new Texture(bitmap);
        Canvas canvas = new Canvas(texture);
        canvas.translate(-deviceR.left, -deviceR.top);
        if (matrix != null) {
            canvas.concat(matrix);
        }
        canvas.drawPixelMapHolderRect(new PixelMapHolder(source), srcR, dstR, paint);
        return bitmap;
    }

    public static PixelMap createPixmapByResourceId(Context context, int resId) {
        InputStream drawableInputStream = null;
        try {
            drawableInputStream = context.getResourceManager().getResource(resId);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(drawableInputStream, null);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(0, 0);
            decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
            return pixelMap;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (drawableInputStream != null) {
                    drawableInputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
