/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm;

import com.watermark.ohoswm.bean.WatermarkImage;
import com.watermark.ohoswm.bean.WatermarkPosition;
import com.watermark.ohoswm.bean.WatermarkText;
import com.watermark.ohoswm.listener.BuildFinishListener;
import com.watermark.ohoswm.utils.BitmapUtils;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;

import java.util.ArrayList;
import java.util.List;

import static com.watermark.ohoswm.utils.Constant.MAX_IMAGE_SIZE;


/**
 * A builder class for setting default structural classes for watermark to use.
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 */
public final class WatermarkBuilder {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "WatermarkBuilder");

    private Context context;
    private PixelMap backgroundImg;
    private boolean isTileMode = false;
    private boolean isLSB = false;
    private boolean resizeBackgroundImg;
    private BuildFinishListener<PixelMap> buildFinishListener = null;

    private WatermarkImage watermarkImage;
    private WatermarkText watermarkText;
    private List<WatermarkText> watermarkTexts = new ArrayList<>();
    private List<WatermarkImage> watermarkBitmaps = new ArrayList<>();

    /**
     * Constructors for WatermarkBuilder
     */
    private WatermarkBuilder(Context context, PixelMap backgroundImg, boolean resizeBackgroundImg) {
        this.context = context;
        this.resizeBackgroundImg = resizeBackgroundImg;
        if (resizeBackgroundImg) {
            this.backgroundImg = BitmapUtils.resizeBitmap(backgroundImg, MAX_IMAGE_SIZE);
        } else {
            this.backgroundImg = backgroundImg;
        }
    }

    private WatermarkBuilder(Context context, Image backgroundImageView, boolean resizeBackgroundImg) {
        this.context = context;
        this.resizeBackgroundImg = resizeBackgroundImg;
        backgroundFromImageView(backgroundImageView);
    }

    private WatermarkBuilder(Context context, int backgroundDrawable, boolean resizeBackgroundImg) {
        this.context = context;
        this.resizeBackgroundImg = resizeBackgroundImg;
        if (resizeBackgroundImg) {
            this.backgroundImg = BitmapUtils.resizeBitmap(BitmapUtils.createPixmapByResourceId(context,backgroundDrawable), MAX_IMAGE_SIZE);
        } else {
            this.backgroundImg = BitmapUtils.createPixmapByResourceId(context,backgroundDrawable);
        }
    }

    private WatermarkBuilder(Context context, PixelMap backgroundImg) {
        this(context, backgroundImg, true);
    }

    private WatermarkBuilder(Context context, Image backgroundImageView) {
        this(context, backgroundImageView, true);
    }

    private WatermarkBuilder(Context context, int backgroundDrawable) {
        this(context, backgroundDrawable, true);
    }


    /**
     * to get an instance form class.
     *
     * @return instance of {@link WatermarkBuilder}
     */
    @SuppressWarnings("PMD")
    public static WatermarkBuilder create(Context context, PixelMap backgroundImg) {
        return new WatermarkBuilder(context, backgroundImg);
    }

    /**
     * to get an instance form class.
     * Load the background image from a {@link Image}。
     *
     * @return instance of {@link WatermarkBuilder}
     */
    @SuppressWarnings("PMD")
    public static WatermarkBuilder create(Context context, Image imageView) {
        return new WatermarkBuilder(context, imageView);
    }

    /**
     * to get an instance form class.
     * Load the background image from a DrawableRes。
     *
     * @return instance of {@link WatermarkBuilder}
     */
    @SuppressWarnings("PMD")
    public static WatermarkBuilder create(Context context, int backgroundDrawable) {
        return new WatermarkBuilder(context, backgroundDrawable);
    }

    /**
     * to get an instance form class.
     * with background image resize option
     *
     * @return instance of {@link WatermarkBuilder}
     */
    @SuppressWarnings("PMD")
    public static WatermarkBuilder create(Context context, PixelMap backgroundImg, boolean resizeBackgroundImg) {
        return new WatermarkBuilder(context, backgroundImg, resizeBackgroundImg);
    }

    /**
     * to get an instance form class.
     * Load the background image from a {@link Image}。
     * with background image resize option
     *
     * @return instance of {@link WatermarkBuilder}
     */
    @SuppressWarnings("PMD")
    public static WatermarkBuilder create(Context context, Image imageView, boolean resizeBackgroundImg) {
        return new WatermarkBuilder(context, imageView, resizeBackgroundImg);
    }

    /**
     * to get an instance form class.
     * Load the background image from a DrawableRes。
     * with background image resize option
     *
     * @return instance of {@link WatermarkBuilder}
     */
    @SuppressWarnings("PMD")
    public static WatermarkBuilder create(Context context, int backgroundDrawable, boolean resizeBackgroundImg) {
        return new WatermarkBuilder(context, backgroundDrawable, resizeBackgroundImg);
    }

    /**
     * Sets the {@link String} as the args
     * which ready for adding to a watermark.
     * Using the default position.
     *
     * @param inputText The text to add.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkText(String inputText) {
        watermarkText = new WatermarkText(inputText);
        return this;
    }

    /**
     * Sets the {@link String} as the args
     * which ready for adding to a watermark.
     * Using the new position.
     *
     * @param inputText The text to add.
     * @param position  The position in the background image.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkText(String inputText,
                                              WatermarkPosition position) {
        watermarkText = new WatermarkText(inputText, position);
        return this;
    }

    /**
     * Sets the {@link String} as the args
     * which ready for adding to a watermark.
     *
     * @param watermarkString The {@link WatermarkText} object.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkText(WatermarkText watermarkString) {
        watermarkText = watermarkString;
        return this;
    }

    /**
     * Sets the {@link String} as the args
     * which ready for adding to a watermark.
     * And, this is a set of Strings.
     *
     * @param watermarkTexts The texts to add.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkTexts(List<WatermarkText> watermarkTexts) {
        this.watermarkTexts = watermarkTexts;
        return this;
    }

    /**
     * Sets the {@link PixelMap} as the args
     * which ready for adding to a background.
     * Using the default position.
     *
     * @param wmImg The image to add.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkImage(PixelMap wmImg) {
        watermarkImage = new WatermarkImage(wmImg);
        return this;
    }

    /**
     * Sets the {@link PixelMap} as the args
     * which ready for adding to a background.
     * Using the new position.
     *
     * @param position The position in the background image.
     * @param wmImg    The bitmap to add into.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkImage(PixelMap wmImg,
                                               WatermarkPosition position) {
        watermarkImage = new WatermarkImage(wmImg, position);
        return this;
    }

    /**
     * Sets the {@link PixelMap} as the args
     * which ready for adding to a background.
     *
     * @param watermarkImg The {@link WatermarkImage} object.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkImage(WatermarkImage watermarkImg) {
        watermarkImage = watermarkImg;
        return this;
    }

    /**
     * Sets the {@link PixelMap} as the args
     * which ready for adding into the background.
     * And, this is a set of bitmaps.
     *
     * @param bitmapList The bitmaps to add.
     * @return This {@link WatermarkBuilder}.
     */
    public WatermarkBuilder loadWatermarkImages(List<WatermarkImage> bitmapList) {
        this.watermarkBitmaps = bitmapList;
        return this;
    }

    /**
     * Set mode to tile. We need to draw watermark over the
     * whole background.
     */
    public WatermarkBuilder setTileMode(boolean tileMode) {
        this.isTileMode = tileMode;
        return this;
    }

    /**
     * set a listener for building progress.
     */
    public void setInvisibleWMListener(boolean isLSB, BuildFinishListener<PixelMap> listener) {
        this.buildFinishListener = listener;
        this.isLSB = isLSB;
        new Watermark(
                context,
                backgroundImg,
                watermarkImage,
                watermarkBitmaps,
                watermarkText,
                watermarkTexts,
                isTileMode,
                true,
                isLSB,
                buildFinishListener
        );
    }


    /**
     * load a bitmap as background image from a ImageView.
     *
     * @param imageView the {@link Image} we need to use.
     */
    private void backgroundFromImageView(Image imageView) {
        imageView.invalidate();
        if (imageView.getBackgroundElement() != null) {
            PixelMapElement drawable = (PixelMapElement) imageView.getBackgroundElement();
            if (resizeBackgroundImg) {
                backgroundImg = BitmapUtils.resizeBitmap(drawable.getPixelMap(), MAX_IMAGE_SIZE);
            } else {
                backgroundImg = drawable.getPixelMap();
            }
        }
    }

    /**
     * let the watermark builder to build a new watermark object
     *
     * @return a new {@link Watermark} object
     */
    public Watermark getWatermark() {
        return new Watermark(
                context,
                backgroundImg,
                watermarkImage,
                watermarkBitmaps,
                watermarkText,
                watermarkTexts,
                isTileMode,
                false,
                isLSB,
                buildFinishListener
        );
    }
}
