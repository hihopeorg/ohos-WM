/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm;


import com.watermark.ohoswm.bean.AsyncTaskParams;
import com.watermark.ohoswm.bean.WatermarkImage;
import com.watermark.ohoswm.bean.WatermarkText;
import com.watermark.ohoswm.listener.BuildFinishListener;
import com.watermark.ohoswm.task.FDWatermarkTask;
import com.watermark.ohoswm.task.LSBWatermarkTask;
import com.watermark.ohoswm.utils.BitmapUtils;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;

import java.util.List;

/**
 * The main class for watermark processing library.
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 */
public class Watermark {

    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "Watermark");

    private WatermarkText watermarkText;
    private WatermarkImage watermarkImg;
    private PixelMap backgroundImg;
    private Context context;
    private PixelMap outputImage;
    private PixelMap canvasBitmap;
    private boolean isTileMode;
    private boolean isInvisible;
    private boolean isLSB;
    private BuildFinishListener<PixelMap> buildFinishListener;

    /**
     * Constructors for WatermarkImage
     */
    @SuppressWarnings("PMD")
    Watermark(Context context,
              PixelMap backgroundImg,
              WatermarkImage watermarkImg,
              List<WatermarkImage> wmBitmapList,
              WatermarkText inputText,
              List<WatermarkText> wmTextList,
              boolean isTileMode,
              boolean isInvisible,
              boolean isLSB,
              BuildFinishListener<PixelMap> buildFinishListener) {

        this.context = context;
        this.isTileMode = isTileMode;
        this.watermarkImg = watermarkImg;
        this.backgroundImg = backgroundImg;
        this.watermarkText = inputText;
        this.isInvisible = isInvisible;
        this.buildFinishListener = buildFinishListener;
        this.isLSB = isLSB;

        canvasBitmap = backgroundImg;
        outputImage = backgroundImg;

        createWatermarkImage(watermarkImg);
        createWatermarkImages(wmBitmapList);
        createWatermarkText(watermarkText);
        createWatermarkTexts(wmTextList);
    }


    /**
     * interface for getting the watermark bitmap.
     *
     * @return {@link PixelMap} in watermark.
     */
    public PixelMap getWatermarkBitmap() {
        return watermarkImg.getImage();
    }

    /**
     * interface for getting the watermark text.
     *
     * @return {@link PixelMap} in watermark.
     */
    public String getWatermarkText() {
        return watermarkText.getText();
    }

    /**
     * Creating the composite image with {@link WatermarkImage}.
     * This method cannot be called outside.
     */
    private void createWatermarkImage(WatermarkImage watermarkImg) {
        if (watermarkImg != null && backgroundImg != null) {
            if (isInvisible) {
                PixelMap scaledWMBitmap = BitmapUtils.resizeBitmap(watermarkImg.getImage()
                        , (float) watermarkImg.getSize(), backgroundImg);
                if (isLSB) {
                    new LSBWatermarkTask(buildFinishListener).execute(
                            new AsyncTaskParams(context, backgroundImg, scaledWMBitmap)
                    );
                } else {
                    new FDWatermarkTask(buildFinishListener).execute(
                            new AsyncTaskParams(context, backgroundImg, scaledWMBitmap)
                    );
                }
            } else {
                Paint watermarkPaint = new Paint();
                PixelMap newBitmap = BitmapUtils.createPixmap(PixelFormat.ARGB_8888
                        , backgroundImg.getImageInfo().size.width
                        , backgroundImg.getImageInfo().size.height);
                Texture texture = new Texture(newBitmap);
                Canvas watermarkCanvas = new Canvas(texture);
                watermarkCanvas.drawPixelMapHolder(new PixelMapHolder(canvasBitmap), 0, 0, watermarkPaint);
                PixelMap scaledWMBitmap = BitmapUtils.resizeBitmap(watermarkImg.getImage(), (float) watermarkImg.getSize(), backgroundImg);
                scaledWMBitmap = adjustPhotoRotation(scaledWMBitmap,
                        (int) watermarkImg.getPosition().getRotation());

                watermarkPaint.setAlpha(watermarkImg.getAlpha());
                if (isTileMode) {
                    PixelMapShader shader = new PixelMapShader(new PixelMapHolder(scaledWMBitmap)
                            , Shader.TileMode.REPEAT_TILEMODE
                            , Shader.TileMode.REPEAT_TILEMODE);
                    watermarkPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
                    Rect bitmapShaderRect = watermarkCanvas.getDeviceClipBounds();
                    watermarkCanvas.drawRect(bitmapShaderRect, watermarkPaint);
                } else {

                    watermarkCanvas.drawPixelMapHolder(new PixelMapHolder(scaledWMBitmap),
                            (float) watermarkImg.getPosition().getPositionX() * backgroundImg.getImageInfo().size.width,
                            (float) watermarkImg.getPosition().getPositionY() * backgroundImg.getImageInfo().size.height,
                            watermarkPaint);
                }
                canvasBitmap = newBitmap;
                outputImage = newBitmap;
            }
        }

    }

    /**
     * Creating the composite image with {@link WatermarkImage}.
     * The input of the method is a set of {@link WatermarkImage}s.
     */
    private void createWatermarkImages(List<WatermarkImage> watermarkImages) {
        if (watermarkImages != null) {
            for (int i = 0; i < watermarkImages.size(); i++) {
                createWatermarkImage(watermarkImages.get(i));
            }
        }
    }

    /**
     * Creating the composite image with  {@link WatermarkText}.
     * This method cannot be called outside.
     */
    private void createWatermarkText(WatermarkText watermarkText) {
        if (watermarkText != null && backgroundImg != null) {
            if (isInvisible) {
                if (isLSB) {
                    new LSBWatermarkTask(buildFinishListener).execute(
                            new AsyncTaskParams(context, backgroundImg, watermarkText)
                    );
                } else {
                    new FDWatermarkTask(buildFinishListener).execute(
                            new AsyncTaskParams(context, backgroundImg, watermarkText)
                    );
                }
            } else {
                Paint watermarkPaint = new Paint();

                PixelMap newBitmap = BitmapUtils.createPixmap(PixelFormat.ARGB_8888
                        , backgroundImg.getImageInfo().size.width
                        , backgroundImg.getImageInfo().size.height);
                Texture texture = new Texture(newBitmap);
                Canvas watermarkCanvas = new Canvas(texture);
                watermarkCanvas.drawPixelMapHolder(new PixelMapHolder(canvasBitmap), 0, 0, watermarkPaint);
                PixelMap scaledWMBitmap = BitmapUtils.textAsBitmap(context, watermarkText);
                scaledWMBitmap = adjustPhotoRotation(scaledWMBitmap,
                        (int) watermarkText.getPosition().getRotation());

                watermarkPaint.setAlpha(watermarkText.getTextAlpha());
                if (isTileMode) {
                    PixelMapShader shader = new PixelMapShader(new PixelMapHolder(scaledWMBitmap)
                            , Shader.TileMode.REPEAT_TILEMODE
                            , Shader.TileMode.REPEAT_TILEMODE);
                    watermarkPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
                    Rect bitmapShaderRect = watermarkCanvas.getDeviceClipBounds();
                    watermarkCanvas.drawRect(bitmapShaderRect, watermarkPaint);
                } else {
                    watermarkCanvas.drawPixelMapHolder(new PixelMapHolder(scaledWMBitmap)
                            , (float) backgroundImg.getImageInfo().size.width
                            , (float) backgroundImg.getImageInfo().size.height,
                            watermarkPaint);
                }
                canvasBitmap = newBitmap;
                outputImage = newBitmap;
            }
        }
    }

    /**
     * Creating the composite image with {@link WatermarkText}.
     * The input of the method is a set of {@link WatermarkText}s.
     */

    private void createWatermarkTexts(List<WatermarkText> watermarkTexts) {
        if (watermarkTexts != null) {
            for (int i = 0; i < watermarkTexts.size(); i++) {
                createWatermarkText(watermarkTexts.get(i));
            }
        }
    }

    /**
     * The interface for getting the output image.
     *
     * @return {@link PixelMap} out bitmap.
     */
    public PixelMap getOutputImage() {
        return outputImage;
    }

    /**
     * Save output png image to local.
     *
     * @param path the output path of image.
     */
    public void saveToLocalPng(String path) {
        BitmapUtils.saveAsPNG(outputImage, path, true);
    }

    /**
     * You can use this function to set the composite
     * image into an ImageView.
     *
     * @param target the target {@link Image}.
     */
    public void setToImageView(Image target) {
        target.setBackground(new PixelMapElement(outputImage));
    }

    /**
     * Adjust the rotation of a bitmap.
     *
     * @param bitmap           input bitmap.
     * @param orientationAngle the orientation angle.
     * @return {@link PixelMap} the new bitmap.
     */
    private PixelMap adjustPhotoRotation(PixelMap bitmap, final int orientationAngle) {
        Matrix matrix = new Matrix();
        matrix.setRotate(orientationAngle,
                (float) bitmap.getImageInfo().size.width / 2, (float) bitmap.getImageInfo().size.height / 2);
        return BitmapUtils.createPixmap(bitmap, 0, 0
                , bitmap.getImageInfo().size.width, bitmap.getImageInfo().size.height
                , matrix, false);
    }

}
