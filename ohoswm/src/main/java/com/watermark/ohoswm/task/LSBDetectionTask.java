/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm.task;


import com.watermark.ohoswm.listener.DetectFinishListener;
import com.watermark.ohoswm.utils.BitmapUtils;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Position;
import ohos.media.image.common.Size;

import static com.watermark.ohoswm.utils.BitmapUtils.getBitmapPixels;
import static com.watermark.ohoswm.utils.BitmapUtils.pixel2ARGBArray;
import static com.watermark.ohoswm.utils.Constant.*;
import static com.watermark.ohoswm.utils.StringUtils.*;

/**
 * This is a task for watermark image detection.
 * In LSB mode, all the task will return a bitmap;
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 */
public class LSBDetectionTask {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "LSBDetectionTask");


    private DetectFinishListener listener;

    public LSBDetectionTask(DetectFinishListener listener) {
        this.listener = listener;
    }

    protected void doInBackground(PixelMap bitmaps) {
        PixelMap markedBitmap = bitmaps;
        DetectionReturnValue resultValue = new DetectionReturnValue();

        if (markedBitmap == null) {
            listener.onFailure(ERROR_BITMAP_NULL);
            return;
        }

        if (markedBitmap.getImageInfo().size.width > MAX_IMAGE_SIZE || markedBitmap.getImageInfo().size.height > MAX_IMAGE_SIZE) {
            listener.onFailure(WARNING_BIG_IMAGE);
            return;
        }

        int[] pixels = getBitmapPixels(markedBitmap);
        int[] colorArray = pixel2ARGBArray(pixels);

        for (int i = 0; i < colorArray.length; i++) {
            colorArray[i] = colorArray[i] % 10;
        }

        replaceNinesJ(colorArray);
        String binaryString = intArrayToStringJ(colorArray);
        String resultString;

        if (binaryString.contains(LSB_TEXT_PREFIX_FLAG) && binaryString.contains(LSB_TEXT_SUFFIX_FLAG)) {
            resultString = getBetweenStrings(binaryString, true, listener);
            resultString = binaryToString(resultString);
            resultValue.setWatermarkString(resultString);
        } else if (binaryString.contains(LSB_IMG_PREFIX_FLAG) && binaryString.contains(LSB_IMG_SUFFIX_FLAG)) {
            binaryString = getBetweenStrings(binaryString, false, listener);
            resultString = binaryToString(binaryString);
            resultValue.setWatermarkBitmap(replaceBitmapColor(BitmapUtils.stringToBitmap(resultString)));
        }
        if (resultValue == null) {
            listener.onFailure(ERROR_DETECT_FAILED);
            return;
        }

        if (resultValue.getWatermarkString() != null &&
                !"".equals(resultValue.getWatermarkString()) ||
                resultValue.getWatermarkBitmap() != null) {
            listener.onSuccess(resultValue);
        } else {
            listener.onFailure(ERROR_DETECT_FAILED);
        }
    }

    public static PixelMap replaceBitmapColor(PixelMap oldBitmap) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        options.size = new Size(oldBitmap.getImageInfo().size.width, oldBitmap.getImageInfo().size.height);
        PixelMap mBitmap = PixelMap.create(oldBitmap, options);
        //循环获得bitmap所有像素点
        int mBitmapWidth = mBitmap.getImageInfo().size.width;
        int mBitmapHeight = mBitmap.getImageInfo().size.height;
        for (int i = 0; i < mBitmapHeight; i++) {
            for (int j = 0; j < mBitmapWidth; j++) {
                int color = mBitmap.readPixel(new Position(j, i));
                if (color == Color.BLACK.getValue()) {
                    mBitmap.writePixel(new Position(j, i), Color.TRANSPARENT.getValue());  //将白色替换成透明色
                }
            }
        }
        return mBitmap;
    }


    public void execute(PixelMap pixelMap) {
        EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
        handler.postTask(new Runnable() {
            @Override
            public void run() {
                doInBackground(pixelMap);
            }
        });
    }

}
