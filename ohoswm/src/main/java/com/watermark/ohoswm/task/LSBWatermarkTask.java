/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm.task;

import com.watermark.ohoswm.bean.AsyncTaskParams;
import com.watermark.ohoswm.bean.WatermarkText;
import com.watermark.ohoswm.listener.BuildFinishListener;
import com.watermark.ohoswm.utils.BitmapUtils;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import static com.watermark.ohoswm.utils.BitmapUtils.getBitmapPixels;
import static com.watermark.ohoswm.utils.BitmapUtils.pixel2ARGBArray;
import static com.watermark.ohoswm.utils.Constant.*;
import static com.watermark.ohoswm.utils.StringUtils.*;

/**
 * This is a background task for adding the specific invisible text
 * into the background image. We don't need to read every pixel's
 * RGB value, we just read the length values that can put our encrypted
 * text in.
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 */
public class LSBWatermarkTask {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "LSBWatermarkTask");

    private BuildFinishListener<PixelMap> listener;

    public LSBWatermarkTask(BuildFinishListener<PixelMap> callback) {
        this.listener = callback;
    }

    protected void doInBackground(AsyncTaskParams params) {
        PixelMap backgroundBitmap = params.getBackgroundImg();
        WatermarkText watermarkText = params.getWatermarkText();
        PixelMap watermarkBitmap = params.getWatermarkImg();
        String watermarkString;

        if (backgroundBitmap == null) {
            listener.onFailure(ERROR_NO_BACKGROUND);
            return;
        }

        // convert the watermark bitmap into a String.
        if (watermarkBitmap != null) {
            watermarkString = BitmapUtils.bitmapToString(watermarkBitmap);
        } else {
            watermarkString = watermarkText.getText();
        }

        if (watermarkString == null) {
            listener.onFailure(ERROR_NO_WATERMARKS);
            return;
        }

        PixelMap outputBitmap = BitmapUtils.createPixmap(backgroundBitmap.getImageInfo().pixelFormat
                , backgroundBitmap.getImageInfo().size.width
                , backgroundBitmap.getImageInfo().size.height);

        int[] backgroundPixels = getBitmapPixels(backgroundBitmap);
        int[] backgroundColorArray = pixel2ARGBArray(backgroundPixels);

        String watermarkBinary = stringToBinary(watermarkString);

        if (watermarkBitmap != null) {
            watermarkBinary = LSB_IMG_PREFIX_FLAG + watermarkBinary + LSB_IMG_SUFFIX_FLAG;
        } else {
            watermarkBinary = LSB_TEXT_PREFIX_FLAG + watermarkBinary + LSB_TEXT_SUFFIX_FLAG;
        }

        int[] watermarkColorArray = stringToIntArray(watermarkBinary);
        if (watermarkColorArray.length > backgroundColorArray.length) {
            listener.onFailure(ERROR_PIXELS_NOT_ENOUGH);
        } else {
            int chunkSize = watermarkColorArray.length;
            int numOfChunks = (int) Math.ceil((double) backgroundColorArray.length / chunkSize);
            for (int i = 0; i < numOfChunks - 1; i++) {
                int start = i * chunkSize;
                for (int j = 0; j < chunkSize; j++) {
                    backgroundColorArray[start + j] = replaceSingleDigit(backgroundColorArray[start + j]
                            , watermarkColorArray[j]);
                }
            }

            for (int i = 0; i < backgroundPixels.length; i++) {
                int color = Color.argb(
                        backgroundColorArray[4 * i],
                        backgroundColorArray[4 * i + 1],
                        backgroundColorArray[4 * i + 2],
                        backgroundColorArray[4 * i + 3]
                );
                backgroundPixels[i] = color;
            }

            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(backgroundBitmap.getImageInfo().size.width, backgroundBitmap.getImageInfo().size.height);
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            initializationOptions.editable = true;
            PixelMap pixelMap = PixelMap.create(backgroundPixels, 0, backgroundBitmap.getImageInfo().size.width
                    , initializationOptions);
            listener.onSuccess(pixelMap);
        }
    }


    public void execute(AsyncTaskParams params) {
        EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
        handler.postTask(new Runnable() {
            @Override
            public void run() {
                doInBackground(params);
            }
        });
    }

}
