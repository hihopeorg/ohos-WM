/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm.task;

import com.watermark.ohoswm.bean.AsyncTaskParams;
import com.watermark.ohoswm.bean.WatermarkText;
import com.watermark.ohoswm.listener.BuildFinishListener;
import com.watermark.ohoswm.utils.BitmapUtils;
import com.watermark.ohoswm.utils.FastDctFft;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;

import static com.watermark.ohoswm.utils.BitmapUtils.*;
import static com.watermark.ohoswm.utils.Constant.*;
import static com.watermark.ohoswm.utils.StringUtils.copyFromIntArray;

/**
 * This is a tack that use Fast Fourier Transform for an image, to
 * build the image and text watermark into a frequency domain.
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 */
public class FDWatermarkTask {

    private BuildFinishListener<PixelMap> listener;

    public FDWatermarkTask(BuildFinishListener<PixelMap> callback) {
        this.listener = callback;
    }

    protected PixelMap doInBackground(AsyncTaskParams... params) {
        PixelMap backgroundBitmap = params[0].getBackgroundImg();
        WatermarkText watermarkText = params[0].getWatermarkText();
        PixelMap watermarkBitmap = params[0].getWatermarkImg();
        Context context = params[0].getContext();

        if (backgroundBitmap == null) {
            listener.onFailure(ERROR_NO_BACKGROUND);
            return null;
        }

        if (watermarkText != null) {
            watermarkBitmap = textAsBitmap(context, watermarkText);
        }

        if (watermarkBitmap == null) {
            listener.onFailure(ERROR_NO_WATERMARKS);
            return null;
        }

        int[] watermarkPixels = getBitmapPixels(watermarkBitmap);
        int[] watermarkColorArray = pixel2ARGBArray(watermarkPixels);
        PixelMap outputBitmap = BitmapUtils.createPixmap(backgroundBitmap.getImageInfo().pixelFormat
                , backgroundBitmap.getImageInfo().size.width
                , backgroundBitmap.getImageInfo().size.height);

        // convert the background bitmap into pixel array.
        int[] backgroundPixels = getBitmapPixels(backgroundBitmap);

        if (watermarkColorArray.length > backgroundPixels.length * 4) {
            listener.onFailure(ERROR_PIXELS_NOT_ENOUGH);
        } else {
            // divide and conquer
            // use fixed chunk size or the size of watermark image.
            if (backgroundPixels.length < watermarkColorArray.length) {
                int[] backgroundColorArray = pixel2ARGBArray(backgroundPixels);
                double[] backgroundColorArrayD = copyFromIntArray(backgroundColorArray);

                FastDctFft.transform(backgroundColorArrayD);

                //TODO: do the operations.

                FastDctFft.inverseTransform(backgroundColorArrayD);
                for (int i = 0; i < backgroundPixels.length; i++) {
                    int color = Color.argb(
                            (int) backgroundColorArrayD[4 * i],
                            (int) backgroundColorArrayD[4 * i + 1],
                            (int) backgroundColorArrayD[4 * i + 2],
                            (int) backgroundColorArrayD[4 * i + 3]
                    );

                    backgroundPixels[i] = color;
                }
            } else {
                // use fixed chunk size or the size of watermark image.
                int numOfChunks = (int) Math.ceil((double) backgroundPixels.length / watermarkColorArray.length);
                for (int i = 0; i < numOfChunks; i++) {
                    int start = i * watermarkColorArray.length;
                    int length = Math.min(backgroundPixels.length - start, watermarkColorArray.length);
                    int[] temp = new int[length];
                    System.arraycopy(backgroundPixels, start, temp, 0, length);
                    double[] colorTempD = copyFromIntArray(pixel2ARGBArray(temp));
                    FastDctFft.transform(colorTempD);

//                    for (int j = 0; j < length; j++) {
//                        colorTempD[4 * j] = colorTempD[4 * j] + watermarkColorArray[j];
//                        colorTempD[4 * j + 1] = colorTempD[4 * j + 1] + watermarkColorArray[j];
//                        colorTempD[4 * j + 2] = colorTempD[4 * j + 2] + watermarkColorArray[j];
//                        colorTempD[4 * j + 3] = colorTempD[4 * j + 3] + watermarkColorArray[j];
//                    }

                    double enhanceNum = 1;

                    // The energy in frequency scaled.
                    for (int j = 0; j < length; j++) {
                        colorTempD[4 * j] = colorTempD[4 * j] * enhanceNum;
                        colorTempD[4 * j + 1] = colorTempD[4 * j + 1] * enhanceNum;
                        colorTempD[4 * j + 2] = colorTempD[4 * j + 2] * enhanceNum;
                        colorTempD[4 * j + 3] = colorTempD[4 * j + 3] * enhanceNum;
                    }

                    //TODO: do the operations.


                    FastDctFft.inverseTransform(colorTempD);

                    for (int j = 0; j < length; j++) {
                        int color = Color.argb(
                                (int) colorTempD[4 * j],
                                (int) colorTempD[4 * j + 1],
                                (int) colorTempD[4 * j + 2],
                                (int) colorTempD[4 * j + 3]
                        );

                        backgroundPixels[start + j] = color;
                    }
                }
            }
            outputBitmap.readPixels(backgroundPixels, 0, backgroundBitmap.getImageInfo().size.width,
                    new Rect(0, 0, backgroundBitmap.getImageInfo().size.width, backgroundBitmap.getImageInfo().size.height));
            return outputBitmap;
        }
        return null;
    }

    public void execute(AsyncTaskParams... params) {
        EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
        handler.postTask(new Runnable() {
            @Override
            public void run() {
                PixelMap pixelMap = doInBackground(params);
                if (listener != null) {
                    if (pixelMap != null) {
                        listener.onSuccess(pixelMap);
                    } else {
                        listener.onFailure(ERROR_CREATE_FAILED);
                    }
                }
            }
        });
    }

    /**
     * Normalize array.
     *
     * @param inputArray The array to be normalized.
     * @return The result of the normalization.
     */
    public double[] normalizeArray(double[] inputArray, double dataHigh,
                                   double dataLow, double normalizedHigh,
                                   double normalizedLow) {
        for (int i = 0; i < inputArray.length; i++) {
            inputArray[i] = ((inputArray[i] - dataLow)
                    / (dataHigh - dataLow))
                    * (normalizedHigh - normalizedLow) + normalizedLow;
        }
        return inputArray;
    }

}