/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm.bean;


import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * It's a wrapper of the watermark text.
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 * @since 29/08/
 */
public class WatermarkText {

    private String text;
    private float alpha = 0.4f;
    private double size = 20;
    private int color = Color.BLACK.getValue();
    private int backgroundColor = Color.TRANSPARENT.getValue();
    private Paint.Style style = Paint.Style.FILL_STYLE;

    private int typeFaceId = 0;
    private float textShadowBlurRadius;
    private float textShadowXOffset;
    private float textShadowYOffset;
    private int textShadowColor = Color.WHITE.getValue();
    // set the default values for the position.
    private WatermarkPosition position = new WatermarkPosition(0, 0, 0);

    /**
     * Constructors for WatermarkText
     */
    public WatermarkText(String text) {
        this.text = text;
    }

    public WatermarkText(String text, WatermarkPosition position) {
        this.text = text;
        this.position = position;
    }

    public WatermarkText(Text textView) {
        textFromTextView(textView);
    }

    public WatermarkText(TextField editText) {
        textFromEditText(editText);
    }

    /**
     * Getters and Setters for those attrs.
     */
    public String getText() {
        return text;
    }

    public float getTextAlpha() {
        return alpha;
    }

    /**
     * @param textAlpha can be set to 0-255.
     */
    public WatermarkText setTextAlpha(float textAlpha) {
        this.alpha = textAlpha;
        return this;
    }

    public WatermarkPosition getPosition() {
        return position;
    }

    public WatermarkText setPosition(WatermarkPosition position) {
        this.position = position;
        return this;
    }

    public double getTextSize() {
        return size;
    }

    /**
     * @param size can be set to normal text size.
     */
    public WatermarkText setTextSize(double size) {
        this.size = size;
        return this;
    }

    public int getTextColor() {
        return color;
    }

    public WatermarkText setTextColor(int color) {
        this.color = color;
        return this;
    }

    public Paint.Style getTextStyle() {
        return style;
    }

    public WatermarkText setTextStyle(Paint.Style style) {
        this.style = style;
        return this;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public WatermarkText setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public float getTextShadowBlurRadius() {
        return textShadowBlurRadius;
    }

    public float getTextShadowXOffset() {
        return textShadowXOffset;
    }

    public float getTextShadowYOffset() {
        return textShadowYOffset;
    }

    public int getTextShadowColor() {
        return textShadowColor;
    }

    public int getTextFont() {
        return typeFaceId;
    }

    /**
     * Use the typeface path to get the text typeface.
     */
    public WatermarkText setTextFont( int typeFaceId) {
        this.typeFaceId = typeFaceId;
        return this;
    }

    public WatermarkText setPositionX(double x) {
        this.position.setPositionX(x);
        return this;
    }

    public WatermarkText setPositionY(double y) {
        this.position.setPositionY(y);
        return this;
    }

    public WatermarkText setRotation(double rotation) {
        this.position.setRotation(rotation);
        return this;
    }

    /**
     * Set the shadow of the text watermark.
     */
    public WatermarkText setTextShadow(final float blurRadius, final float shadowXOffset,
                                       final float shadowYOffset,  final int shadowColor) {
        this.textShadowBlurRadius = blurRadius;
        this.textShadowXOffset = shadowXOffset;
        this.textShadowYOffset = shadowYOffset;
        this.textShadowColor = shadowColor;
        return this;
    }

    /**
     * load a string text as watermark text from a {@link Text}.
     *
     * @param textView the {@link Text} we need to use.
     */
    private void textFromTextView(Text textView) {
        this.text = textView.getText().toString();
    }

    /**
     * load a string text as watermark text from a {@link TextField}.
     *
     * @param editText the {@link TextField} we need to use.
     */
    private void textFromEditText(TextField editText) {
        this.text = editText.getText().toString();
    }

}
