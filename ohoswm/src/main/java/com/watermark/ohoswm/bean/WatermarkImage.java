/*
 *    Copyright 2018 Yizheng Huang
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.watermark.ohoswm.bean;


import com.watermark.ohoswm.utils.BitmapUtils;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

import static com.watermark.ohoswm.utils.Constant.MAX_IMAGE_SIZE;


/**
 * It's a wrapper of the watermark image.
 *
 * @author huangyz0918 (huangyz0918@gmail.com)
 * @since 29/08/2018
 */
public class WatermarkImage {
    private PixelMap image;
    private int imageDrawable;
    private float alpha = 0.3f;
    private Context context;
    private double size = 0.2;
    // set the default values for the position.
    private WatermarkPosition position = new WatermarkPosition(0, 0, 0);

    /**
     * Constructors for WatermarkImage.
     * since we use the mobile to calculate the image, the image cannot be to large,
     * we set the maxsize of an image to 1024x1024.
     */
    public WatermarkImage(PixelMap image) {
        this.image = BitmapUtils.resizeBitmap(image, MAX_IMAGE_SIZE);
    }

    public WatermarkImage(Context context, int imageDrawable, WatermarkPosition position) {
        this.imageDrawable = imageDrawable;
        this.position = position;
        this.context = context;
        this.image = getPixelMapFromDrawable(imageDrawable);
    }

    public WatermarkImage(Context context,  int imageDrawable) {
        this.imageDrawable = imageDrawable;
        this.context = context;
        this.image = getPixelMapFromDrawable(imageDrawable);
    }

    public WatermarkImage(PixelMap image, WatermarkPosition position) {
        this.image = BitmapUtils.resizeBitmap(image, MAX_IMAGE_SIZE);
        this.position = position;
    }

    public WatermarkImage(Image imageView) {
        watermarkFromImageView(imageView);
    }

    /**
     * Getters and Setters for those attrs.
     */
    public PixelMap getImage() {
        return image;
    }

    public float getAlpha() {
        return alpha;
    }

    public WatermarkPosition getPosition() {
        return position;
    }

    public WatermarkImage setPosition(WatermarkPosition position) {
        this.position = position;
        return this;
    }

    public double getSize() {
        return size;
    }

    /**
     * @param size can be set to 0-1 as the proportion of
     *             background image.
     */
    public WatermarkImage setSize( double size) {
        this.size = size;
        return this;
    }

    public int getImageDrawable() {
        return imageDrawable;
    }

    public WatermarkImage setImageDrawable( int imageDrawable) {
        this.imageDrawable = imageDrawable;
        return this;
    }

    public WatermarkImage setPositionX( double x) {
        this.position.setPositionX(x);
        return this;
    }

    public WatermarkImage setPositionY( double y) {
        this.position.setPositionY(y);
        return this;
    }

    public WatermarkImage setRotation(double rotation) {
        this.position.setRotation(rotation);
        return this;
    }

    /**
     * @param imageAlpha can be set to 0-1.
     */
    public WatermarkImage setImageAlpha(float imageAlpha) {
        this.alpha = imageAlpha;
        return this;
    }

    /**
     * load a PixelMap as watermark image from a ImageView.
     *
     * @param imageView the ImageView we need to use.
     */
    private void watermarkFromImageView(Image imageView) {
        imageView.invalidate();
        PixelMapElement drawable = (PixelMapElement) imageView.getBackgroundElement();
        // set the limitation of input PixelMap.
        this.image =BitmapUtils.resizeBitmap(drawable.getPixelMap(), MAX_IMAGE_SIZE);
    }

    private PixelMap getPixelMapFromDrawable( int imageDrawable) {
        PixelMap pixelMap = null;
        try {
            Resource bgResource =   context .getResourceManager().getResource(imageDrawable);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(bgResource, srcOpts);
            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            pixelMap = imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return BitmapUtils.resizeBitmap(pixelMap, MAX_IMAGE_SIZE);
    }
}
